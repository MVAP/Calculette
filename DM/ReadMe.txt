les points réalisés dans le projet :
   1- Calcul d'expression arithmétique sur la MVàP
   2- Traitement des variables Globales
   3- Traitement des entrées / sorties
   4- Les opérations d’incrémentations et les blocs
   5- Traitement du « while »
   6- Traitement des conditions de base (< , > ,==, >=, <=, <>,!=)
   7- Traitement des expressions logiques (|| , && , !)
   8- Branchements (if else)
   9- Traitement du « for »
   10- Traitement des Fonctions
       - Fonction sans argument
       - Fonction avec paramètres 
       - Fonction avec valeur de retour 
       - Gestion des Variables locales
  
   11- Support minimal des flottants
   
Autres Améliorations : 
   la gestion des differents types des variables , dans la comparaison , le calculs , les     valeurs de retours.
   affichage d'un un message d'erreur au cas où l'utilisateurs mélange entre les types dans une expression .
    Réalisation du casting entre les flottants et les entiers
    On a commencé à mettre en oeuvre le type booléen, mais il nous manque encore la transformations de toutes les conditions en forme des expressions.
    
   
   
