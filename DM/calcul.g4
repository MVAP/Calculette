grammar calcul; 

@parser::members {
private int _cur_label = 1;
    // générateur de nom d'étiquettes pour les boucles
    private String getNewLabel() { return "Label" +(_cur_label++); }
     private TablesSymboles tablesSymboles = new TablesSymboles();
     private String evalexpr ( String op) {
        if ( op.equals("*") ){
            return "MUL\n";
        } else if ( op.equals("+") ){
            return "ADD\n";
        } else if ( op.equals("-") ){
            return "SUB\n";
        } else if(op.equals("/")){
            return "DIV\n";
        }
         else {
           System.err.println("Opérateur arithmétique incorrect : '"+op+"'");
           throw new IllegalArgumentException("Opérateur arithmétique incorrect : '"+op+"'");
        }
    }
    
    private String evalexprcomp ( String op) {
       if(op.equals("=="))
        {
            return "EQUAL \n";
        }
        else if(op.equals("!=") || op.equals("<>"))
        {
            return "NEQ \n";
        }
         else if(op.equals(">"))
        {
            return "SUP \n";
        }
         else if(op.equals("<"))
        {
            return "INF \n";
        }
         else if(op.equals(">="))
        {
            return "SUPEQ \n";
        }
         else if(op.equals("<="))
        {
            return "INFEQ \n";
        }
         else {
           System.err.println("Opérateur arithmétique incorrect : '"+op+"'");
           throw new IllegalArgumentException("Opérateur arithmétique incorrect : '"+op+"'");
        }
    }
        
    
}

start
    : calcul EOF;

calcul returns [ String code ]

@init{ $code = new String(); }   // On initialise code, pour l'utiliser comme accumulateur
@after{ System.out.println($code); } // On affiche l’ensemble du code produit

    : (decl { $code += $decl.code; })*        
        { $code += "  JUMP Main\n"; }
        NEWLINE*
        
        (fonction { $code += $fonction.code; })* 
        NEWLINE*
        
        { $code += "LABEL Main\n"; }
        (instruction { $code += $instruction.code; })*

        { $code += "  HALT\n"; } 
    
    ;
    
bloc returns [ String code ] 

 @init{ $code = new String(); } 
    : '{' 
         NEWLINE?

        (decl { $code += $decl.code; })*

        NEWLINE*

	 (  instruction  {  $code += $instruction.code;  } )*
            
      '}'  
      NEWLINE*
    ;

instruction returns [ String code ]
    : expression finInstruction
     {
            $code = $expression.code +"\n";
     }
     | boucle 
     {
         $code = $boucle.code + "\n" ;
     }
     | fonction 
     {
        $code=$fonction.code;

     }
      | RETURN expression finInstruction    
     {
       //$code = $expression.code + (vi.type.equals("int") ? "" : "STOREL " + (tablesSymboles.getReturn().address+1) +"\n")+"STOREL " + tablesSymboles.getReturn().address + "\n" ;
       
        VariableInfo vi=tablesSymboles.getReturn();
        if(vi==null)
        {   
           
            $code = "";
        }
        else
        {
            //$code=$expression.code+"STOREL"+vi.address+"\n";
            $code  = $expression.code;
            if(vi.type.equals("double"))
        {
             $code += "STOREL "+(vi.address+1)+"\n";
        }
             $code += "STOREL "+(vi.address)+"\n";
        

            $code+="RETURN \n";


        }
       
       
     }
     | 'input' '(' IDENTIFIANT ')' 
     {     
            VariableInfo vi = tablesSymboles.getVar($IDENTIFIANT.text);
            $code = "READ" +(vi.type.equals("int") ? "\n" : "F\n"+"STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+(vi.address+1)+"\n");
            $code+= "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")  + vi.address + "\n";
            
     }
     | 'print' '(' a=expression ')' 
     {
            
            $code=$a.code;
            $code+="WRITE"+($a.type.equals("int") ? "\n" : "F\nPOP\n");
            $code+="POP\n"; 
     }
     | assignation finInstruction
     { 
      $code = $assignation.code +"\n";
     }
    
     | finInstruction
     {
            $code="";
     }
    ;

expression returns [ String code , String type]
    :
     '-' a=expression 
    {
      $code = $a.code+"PUSHI -1\n" + "MUL\n";
      $type=$a.type;
    }
    | '(' a=expression ')' 
    {
      $code = $a.code ;
       $type=$a.type;
    }
    | a=expression op=('*'|'/') b=expression
    {
      if(!$a.type.equals($b.type))
        System.err.println("erreur TYPE DIFFERENT !");
      $type=$a.type;
      $code = $a.code + $b.code + ( $a.type.equals("double") ? "F"+ evalexpr($op.text) : evalexpr($op.text));
    }
    | a=expression op=('+'|'-') b=expression
    {
      if(!$a.type.equals($b.type))
        System.err.println("erreur TYPE DIFFERENT !");
      $type=$a.type;
      $code = $a.code + $b.code + ( $a.type.equals("double") ? "F"+ evalexpr($op.text) : evalexpr($op.text));
    }
    | IDENTIFIANT
    {
       VariableInfo vi = tablesSymboles.getVar($IDENTIFIANT.text);
       $type=vi.type;
       $code = "PUSH"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") + vi.address ;
       $code+= (vi.type.equals("double") ? "\nPUSH"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") + (vi.address+1) + "\n" : "\n");
    }
    | IDENTIFIANT '(' ')'                  // appel de fonction
    {
      String vi = tablesSymboles.getFunction($IDENTIFIANT.text);
      $type=vi;
     
      $code = (vi.equals("double") ? "PUSHI 0\n" : "");
      $code += "PUSHI 0\n"+ "CALL " + $IDENTIFIANT.text +"\n";
    }
    | IDENTIFIANT '(' args ')'                  // appel de fonction
    {
      String vi = tablesSymboles.getFunction($IDENTIFIANT.text);
      $type=vi;
      $code = (vi.equals("double") ? "PUSHI 0\n" : "");
      $code += "PUSHI 0\n"+$args.code + "CALL " + $IDENTIFIANT.text +"\n";
      for(int i=0;i<$args.size ;i++)
       $code +="POP\n";
    } 
    | '(' TYPE  ')' a=expression                 // casting
    {
      $code = $a.code ;
      if($a.type.equals("double") && $TYPE.text.equals("int"))
        $code += "FTOI\n";
       if($a.type.equals("int") && $TYPE.text.equals("double"))
        $code += "ITOF\n";
         
    }
    | DOUBLE
    {
      $code = "PUSHF "+$DOUBLE.text+"\n";
      $type="double";
    }
    | ENTIER
    {
      $code = "PUSHI " + $ENTIER.int + "\n";
      $type="int";
    }
   
    
    ;
    
    
decl returns [ String code ]
    :
        TYPE IDENTIFIANT finInstruction
        {
            
            //$code = "PUSHI " + 0 + "\n";
            $code= "PUSH"+ ($TYPE.text.equals("int") ? "I 0" : "F 0.0") +"\n" ;
            tablesSymboles.addVarDecl($IDENTIFIANT.text,$TYPE.text);

        }
        |  TYPE IDENTIFIANT '=' a=expression finInstruction
        {

            tablesSymboles.addVarDecl($IDENTIFIANT.text,$TYPE.text);
            VariableInfo vi = tablesSymboles.getVar($IDENTIFIANT.text);
           //$code = "PUSHI " + 0 + "\n" + $a.code + "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")  + vi.address + "\n";;
           if(!$a.type.equals(vi.type))
            System.err.println("erreur TYPE DIFFERENT !");
            $code= "PUSH"+ ($TYPE.text.equals("int") ? "I 0" : "F 0.0") +"\n" ;
            $code+=$a.code;
            $code+=(vi.type.equals("double") ? "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+(vi.address+1) + "\n" : "") ;
            $code+="STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") +vi.address+ "\n";
            
        }
    ;

assignation returns [ String code ] 
    : IDENTIFIANT '=' a=expression
     {  
            VariableInfo vi = tablesSymboles.getVar($IDENTIFIANT.text);
            //$code = $a.code + "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")  + vi.address + "\n";
             if(!$a.type.equals(vi.type))
              System.err.println("erreur TYPE DIFFERENT !");
            $code=$a.code;
            $code+=(vi.type.equals("double") ? "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+(vi.address+1) + "\n" : "") ;
            $code+="STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+vi.address+ "\n";
            
        
     }
     | IDENTIFIANT '+''=' a=expression
     {  
        VariableInfo vi = tablesSymboles.getVar($IDENTIFIANT.text);

        //$code = $a.code "PUSH"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") + vi.address + "\n"+ "ADD\n"+ "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")  + vi.address +"\n";
        if(!$a.type.equals(vi.type))
          System.err.println("erreur TYPE DIFFERENT !");
        
        $code = "PUSH"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") + vi.address ;
        $code+= (vi.type.equals("double") ? "\nPUSH"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ") + (vi.address+1) + "\n" : "\n");
        $code+=$a.code;
        $code+="ADD\n";
        $code+=(vi.type.equals("double") ? "STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+(vi.address+1) + "\n" : "") ;
        $code+="STORE"+(vi.scope==VariableInfo.Scope.GLOBAL ? "G " : "L ")+vi.address+ "\n";
     
     }     
    ;
    
boucle returns [String code]
    : 'while' '(' c=conditions ')' b=bloc
    {   
        String labeld = getNewLabel();
        String labelf = getNewLabel();
        $code = "LABEL " + labeld +"\n" + $c.code  + "JUMPF "+ labelf+"\n"
                + $b.code + "JUMP "+ labeld+"\n" + "LABEL "+ labelf+ "\n";
    }
   
    |'while' '(' c=conditions ')' i=instruction
    {   String labeld = getNewLabel();
        String labelf = getNewLabel();
        $code = "LABEL " + labeld +"\n" + $c.code  + "JUMPF "+ labelf+"\n"
                + $i.code + "JUMP "+ labeld+"\n" + "LABEL "+ labelf+ "\n";
    }
     NEWLINE*
    |  'if' '(' c=conditions ')' ithen=instruction (finInstruction)? 'else' i=instruction
    {
         String labelif = getNewLabel();
         String labelelse = getNewLabel();
         $code =  $c.code  + "JUMPF "+ labelelse+"\n"
                + $ithen.code +"JUMP "+ labelif+"\n" +"LABEL "+ labelelse+"\n"  + $i.code+ "LABEL "+ labelif+"\n" ;
    
    }
    |  'if' '(' c=conditions ')' blocthen=bloc 'else' ielse=instruction
    {
         String labelif = getNewLabel();
         String labelelse = getNewLabel();
         $code =  $c.code  + "JUMPF "+ labelelse+"\n"
                + $blocthen.code +"JUMP "+ labelif+"\n" +"LABEL "+ labelelse+"\n"  + $ielse.code+ "LABEL "+ labelif+"\n" ;
    
    }
     |  'if' '(' c=conditions ')' blocthen=bloc 'else' blocelse=bloc
    {
         String labelif = getNewLabel();
         String labelelse = getNewLabel();
         $code =  $c.code  + "JUMPF "+ labelelse+"\n"
                + $blocthen.code +"JUMP "+ labelif+"\n" +"LABEL "+ labelelse+"\n"  + $blocelse.code+ "LABEL "+ labelif+"\n" ;
    
    }
    | 'if' '(' c=conditions ')' instructionthen=instruction 
    {    
         String labelelse = getNewLabel();
         $code =  $c.code  + "JUMPF "+ labelelse+"\n"
                + $instructionthen.code  +"LABEL "+ labelelse+"\n"  ;
    }
    |   'if' '(' c=conditions ')' blocthen=bloc 
    {
         
         String labelelse = getNewLabel();
         $code =  $c.code  + "JUMPF "+ labelelse+"\n"
                + $blocthen.code  +"LABEL "+ labelelse+"\n"  ;
    }
   |'for' '(' init=assignation ';' cfor=condition ';' incr=assignation ')' i=instruction
   {
        String labeld = getNewLabel();
        String labelf = getNewLabel();
        $code = $init.code+ "LABEL " + labeld +"\n" + $cfor.code  + "JUMPF "+ labelf+"\n"
                + $i.code + $incr.code +"JUMP "+ labeld+"\n" + "LABEL "+ labelf+ "\n";
   }
   |'for' '(' init=assignation ';' cfor=condition ';' incr=assignation ')' b=bloc
   {
        String labeld = getNewLabel();
        String labelf = getNewLabel();
        $code = $init.code+ "LABEL " + labeld +"\n" + $cfor.code  + "JUMPF "+ labelf+"\n"
                + $b.code + $incr.code +"JUMP "+ labeld+"\n" + "LABEL "+ labelf+ "\n";
   }
   
    ;

condition returns [String code]
    : 'true'  
     {
       $code = "PUSHI 1\n"; 
     }
     | 'false' 
     { 
       $code = "PUSHI 0\n"; 
     }
     | a=expression com=COMPARAISON b=expression
     {  
        if(!$a.type.equals($b.type))
        System.err.println("erreur TYPE DIFFERENT !");
        $code = $a.code + $b.code + ($a.type.equals("int")?  evalexprcomp($com.text) : "F"+ evalexprcomp($com.text));
        //$code = $a.code + $b.code + evalexprcomp($com.text);
     }
    ;

conditions returns [String code]
    : condition
     {
        $code = $condition.code ;
     }
     |'!' condition 
     { 
       $code = "PUSHI 1\n" + $condition.code + "SUB\n"; 
     }
     |'!' '(' c=conditions ')' 
     { 
       $code = "PUSHI 1\n" + $c.code + "SUB\n"; 
     }
     |c=conditions '&&' co=conditions 
     {  String labelf = getNewLabel();
        $code = $c.code + $co.code +"ADD\n" + "PUSHI 2\n"+ "EQUAL\n";
     }
     |c=conditions '||' co=conditions 
     {
       String labelf = getNewLabel();
       $code = $c.code + $co.code + "ADD\n" + "PUSHI 1\n"+"SUPEQ\n" ;
     }
     ; 	 
     
params
    : TYPE IDENTIFIANT
        {
          tablesSymboles.addParam($IDENTIFIANT.text,$TYPE.text); 
        }
        ( ',' TYPE IDENTIFIANT
            {
              tablesSymboles.addParam($IDENTIFIANT.text,$TYPE.text); 
            }
        )*
    ;

fonction returns [ String code ]
@init{ tablesSymboles.enterFunction(); }   // Allocation Memoire 
@after{ tablesSymboles.exitFunction(); } // Free Memoire
    : TYPE IDENTIFIANT  
        {      $code = "LABEL " + $IDENTIFIANT.text+"\n";
	        tablesSymboles.addFunction($IDENTIFIANT.text, $TYPE.text);
	      }
        '(' params ? ')' bloc 
        {  
          
          $code+=$bloc.code;
	  $code += "RETURN\n";  //  Return "de sécurité"      
        }
      NEWLINE*
    ;

args returns [ String code, int size] @init{ $code = new String(); $size = 0; }
    : ( expression
    {
      $code = $expression.code ;
      if($expression.type.equals("double")){
                $size+=2;
        }else{
               $size++;}
    }
    ( ',' expression
    {
      $code += $expression.code;   
      if($expression.type.equals("double")){
                $size+=2;
        }else{
            $size++;}
     }
    )*
    )?
    ;


boole returns [ String code ] 
  : conditions 
    { $code = $conditions.code;
    };
    
 
finInstruction : ( NEWLINE | ';' )+ ;

// lexer
NEWLINE : '\r'? '\n' ;

WS :   (' '|'\t')+ -> skip  ;

TYPE : 'int' | 'double'| 'bool' ;

RETURN: 'return' ;

BREAK : 'Break' ;

CONTINUE : 'continue' ;

IDENTIFIANT : ('a'..'z'|'A'..'Z')('a'..'z'|'0'..'9' |'_')* ;

OPLOGIQUE : '||' | '&&' | '!' ;

COMPARAISON : '>'|'<'| '>=' |'<=' |'=='|'!='  ;

ENTIER : ('0'..'9')+  ;

DOUBLE : ( ENTIER '.' ENTIER) |(ENTIER '.') | ('.' ENTIER);

COMMENT : (('/*' .*? '*/') | ('%' ~('\r'|'\n')* )) -> skip;

//PARENTHESES : ('(' expr *? ')')* ;

UNMATCH : . -> skip ;
